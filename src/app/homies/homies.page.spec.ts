import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomiesPage } from './homies.page';

describe('HomiesPage', () => {
  let component: HomiesPage;
  let fixture: ComponentFixture<HomiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomiesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
